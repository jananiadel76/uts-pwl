<?php
session_start();
if (empty($_SESSION['cart']["arrCart"]))
	$_SESSION['cart']["arrCart"]=array(); 					
?>

<!DOCTYPE html>
<html>
<head>
	<title>Toko Music Jewelry</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container-fluid">
				<a class="navbar-brand" href="list-product.php">Home</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item">
					<a class="nav-link active" aria-current="page" href="cart-disp.php">Cart</a>
					</li>
				</ul>
				</div>
			</div>
		</nav>
<h2>Toko Musik Jewelry</h2>
<br><br>
<div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="img/gitar.jpg" target="_blank">
          <img src="img/gitar.jpg" alt="Lights" style="width: 300px;height: 200px;">
		  	<center> 
				<a href="addCart.php?brg=gitar&hrg=1300000&jml=1">Gitar</a><br />
				Rp 1.300.000,-
			</center>        
        </a>
      </div>
    </div>
	<div class="col-md-4">
      <div class="thumbnail">
			<a href="img/drum.jpg" target="_blank">
				<img src="img/drum.jpg" alt="Lights" style="width: 300px;height: 200px;">
				<center> 
					<a href="addCart.php?brg=drum&hrg=10000000&jml=1">Drum</a><br />
					Rp 10.000.000,-
				</center>			
			</a>
      </div>
    </div>
	<div class="col-md-4">
      <div class="thumbnail">
			<a href="img/bass.jpg" target="_blank">
				<img src="img/bass.jpg" alt="Lights" style="width: 300px;height: 200px;">
				<center> 
					<a href="addCart.php?brg=bass&hrg=2400000&jml=1">Bass</a><br />
					Rp 2.400.000,-
				</center>			
			</a>
      </div>
    </div>
	<div class="col-md-4">
      <div class="thumbnail">
			<a href="img/terompet.jpg" target="_blank">
				<img src="img/terompet.jpg" alt="Lights" style="width: 300px;height: 200px;">
				<center> 
					<a href="addCart.php?brg=terompet&hrg=1800000&jml=1">Terompet</a><br />
					Rp 1.800.000,-
				</center>			
			</a>
      </div>
    </div>
	<div class="col-md-4">
      <div class="thumbnail">
			<a href="img/mic.jpg" target="_blank">
				<img src="img/mic.jpg" alt="Lights" style="width: 300px;height: 200px;">
				<center> 
					<a href="addCart.php?brg=mic&hrg=600000&jml=1">Mic</a><br />
					Rp 600.000,-
				</center>			
			</a>
      </div>
    </div>
	<div class="col-md-4">
      <div class="thumbnail">
			<a href="img/drumpad.jpg" target="_blank">
				<img src="img/drumpad.jpg" alt="Lights" style="width: 300px;height: 200px;">
				<center> 
					<a href="addCart.php?brg=drumpad&hrg=4800000&jml=1">Drumpad</a><br />
					Rp 4.800.000,-
				</center>
			</a>
      </div>
    </div>
</div>    
</body>
</html>
